#!/bin/bash
VERSION='5.18.14'
cp img/cover.png ..
pandoc -f html -t markdown -o LKMPG-${VERSION}.md LKMPG-${VERSION}.html
pandoc -f markdown -t rst -o LKMPG-${VERSION}.rst LKMPG-${VERSION}.md
git add LKMPG-${VERSION}.md
git add LKMPG-${VERSION}.rst
if [ -f /usr/bin/ebook-convert ]; then
    ebook-convert LKMPG-${VERSION}.html LKMPG-${VERSION}.epub --cover img/cover.png --authors "Peter Jay Salzman, Michael Burian, Ori Pomerantz, Bob Mottram, Jim Huang" --language "English" --tags "kernel, programming, C, linux" --comments "Learn how to write modules for the Linux kernel" --title "The Linux Kernel Module Programmers Guide"
    cp LKMPG-${VERSION}.epub ../lkmpg.epub
else
    echo 'ebook-convert command not found'
    if [ -f /usr/bin/flatpak ]; then
        if ! flatpak --command="sh" run com.calibre_ebook.calibre \
             -c "ebook-convert LKMPG-${VERSION}.html LKMPG-${VERSION}.epub --cover img/cover.png --authors \"Peter Jay Salzman, Michael Burian, Ori Pomerantz, Bob Mottram, Jim Huang\" --language \"English\" --tags \"kernel, programming, C, linux\" --comments \"Learn how to write modules for the Linux kernel\" --title \"The Linux Kernel Module Programmers Guide\""; then
            exit 2
        fi
        cp LKMPG-${VERSION}.epub ../lkmpg.epub
    fi
fi
